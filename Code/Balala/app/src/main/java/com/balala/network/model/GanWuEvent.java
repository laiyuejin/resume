package com.balala.network.model;

import java.util.List;


public class GanWuEvent {
    public final String message;
    public final List<GanWuItem> items;


    public GanWuEvent(String message , List<GanWuItem> items) {
        this.message = message;
        this.items = items;
    }
}
