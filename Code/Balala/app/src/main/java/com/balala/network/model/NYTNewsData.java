package com.balala.network.model;

import java.util.List;

/**
 * Created by jin on 5/23/2016.
 */
public class NYTNewsData {
    public String status;
    public List<NYTNewsItem> results;
    public NYTNewsData(String status,List<NYTNewsItem> results){
        this.status = status;
        this.results = results;
    }
}
