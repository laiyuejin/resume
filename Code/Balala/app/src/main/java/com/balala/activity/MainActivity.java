package com.balala.activity;


import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.balala.R;
import com.balala.adapter.TabFragmentAdapter;
import com.balala.base.BaseActivity;
import com.balala.fragment.AndroidFragment;
import com.balala.fragment.PrettyFragment;
import com.balala.fragment.ScienceNewsFragment;
import com.balala.fragment.TechFragment;
import com.balala.globle.Constant;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.Types.BoomType;
import com.nightonke.boommenu.Types.ButtonType;
import com.nightonke.boommenu.Types.PlaceType;
import com.nightonke.boommenu.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 主页面
 */
public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.tabLayout)
    TabLayout mTab;
    @Bind(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @Bind(R.id.fabBtn)
    BoomMenuButton fabBtn;
    @Bind(R.id.rootLayout)
    CoordinatorLayout rootLayout;
    @Bind(R.id.viewPager)
    ViewPager viewPager;
    @Nullable
    @Bind(R.id.app_bar_layout)
    AppBarLayout mAppBar;
    @Bind(R.id.navigation)
    NavigationView mNavigation;
    private Context context;
    ActionBarDrawerToggle drawerToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        context = this;
        initializeToolbar();
        initializeTab();
        initView();
    }

    private void initializeToolbar() {
        setSupportActionBar(mToolbar);
        mToolbar.setTitle("babala");
//        if (Build.VERSION.SDK_INT >= 21) {
////            mAppBar.setElevation(10.6f);
//        }
    }

    private void initializeTab() {
        mTab.addTab(mTab.newTab().setText(Constant.PRETTY));
        mTab.addTab(mTab.newTab().setText(Constant.ANDROID));
        mTab.addTab(mTab.newTab().setText(Constant.OTHER));
        mTab.addTab(mTab.newTab().setText(Constant.SCIENCE));
        List<String> tabList = new ArrayList<>();
        tabList.add(Constant.PRETTY);
        tabList.add(Constant.ANDROID);
        tabList.add(Constant.OTHER);
        tabList.add(Constant.SCIENCE);

        List<Fragment> fragmentList = new ArrayList<>();
        for (int i = 0; i < tabList.size(); i++) {
            if (i == 0) {
                Fragment fragment = new PrettyFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Constant.FRAGMENT_TYPE, Constant.PRETTY);
                fragment.setArguments(bundle);
                fragmentList.add(fragment);
            } else if (i == 1) {
                Fragment fragment = new AndroidFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Constant.FRAGMENT_TYPE, Constant.ANDROID);
                fragment.setArguments(bundle);
                fragmentList.add(fragment);
            } else if (i == 2) {
                Fragment fragment = TechFragment.newInstance("拓展资源");
                fragmentList.add(fragment);
            } else {
                Fragment fragment = new ScienceNewsFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Constant.FRAGMENT_TYPE, Constant.SCIENCE);
                fragment.setArguments(bundle);
                fragmentList.add(fragment);
            }

        }
        TabFragmentAdapter fragmentAdapter = new TabFragmentAdapter(getSupportFragmentManager(), fragmentList, tabList);
        viewPager.setAdapter(fragmentAdapter);//给ViewPager设置适配器
        mTab.setupWithViewPager(viewPager);//将TabLayout和ViewPager关联起来。
        mTab.setTabsFromPagerAdapter(fragmentAdapter);//给Tabs设置适配器
    }

    private void initView() {
        drawerToggle = new ActionBarDrawerToggle(MainActivity.this, drawerLayout, R.string.OPEN_DRAWER_CONTENT_DESC_RES
                , R.string.OPEN_DRAWER_CONTENT_DESC_RES);
        drawerLayout.setDrawerListener(drawerToggle);
//        getSupportActionBar().setHomeButtonEnabled(true);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Drawable[] drawables = new Drawable[3];
        int[] drawablesResource = new int[]{
                R.drawable.mark,
                R.drawable.info,
                R.drawable.like,
        };

        for (int i = 0; i < 3; i++)
            drawables[i] = ContextCompat.getDrawable(this, drawablesResource[i]);

        String[] strings = new String[]{
                "Mark",
                "Info",
                "Like",
        };

        int[][] colors = new int[3][2];
        for (int i = 0; i < 3; i++) {
            colors[i][1] = GetRandomColor();
            colors[i][0] = Util.getInstance().getPressedColor(colors[i][1]);
        }

        // Now with Builder, you can init BMB more convenient
        new BoomMenuButton.Builder()
                .subButtons(drawables, colors, strings)
                .button(ButtonType.CIRCLE)
                .boom(BoomType.HORIZONTAL_THROW_2)
                .place(PlaceType.CIRCLE_3_1)
                .boomButtonShadow(Util.getInstance().dp2px(2), Util.getInstance().dp2px(2))
                .subButtonsShadow(Util.getInstance().dp2px(2), Util.getInstance().dp2px(2))
                .shareStyle(3f, GetRandomColor(), GetRandomColor())
                .onSubButtonClick(new BoomMenuButton.OnSubButtonClickListener() {
                    @Override
                    public void onClick(int buttonIndex) {
                        Toast.makeText(context, "click" + buttonIndex, 1000).show();
                    }
                })

                .init(fabBtn);

        mNavigation.setNavigationItemSelectedListener(this);
    }


    public int GetRandomColor() {
        Random random = new Random();
        int p = random.nextInt(Colors.length);
        return Color.parseColor(Colors[p]);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item))
            return true;
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                return true;
            case R.id.action_about:
                Intent intent = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_search:
                return true;
            case R.id.action_share:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.navItem1:
                Intent intent1 = new Intent(MainActivity.this, MainActivity.class);
                startActivity(intent1);
                break;
            case R.id.navItem2:
                Toast.makeText(MainActivity.this, "coming soon", Toast.LENGTH_SHORT).show();
                break;
            case R.id.navItem3:
                Intent intent3 = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(intent3);
                break;
            case R.id.navItem4:
                Toast.makeText(MainActivity.this, "comming soon", Toast.LENGTH_SHORT).show();
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private String[] Colors = {
            "#00BCD4",
            "#009688",
            "#4CAF50",
            "#8BC34A",
            "#CDDC39",
            "#FFEB3B",
            "#FFC107",
            "#FF9800",
            "#FF5722",
            "#795548",
            "#9E9E9E",
            "#607D8B"};
}