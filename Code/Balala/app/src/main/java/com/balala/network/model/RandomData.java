package com.balala.network.model;

import java.util.List;


public class RandomData {
    public List<News> results;
    private boolean error;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
