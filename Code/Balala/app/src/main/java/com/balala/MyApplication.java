package com.balala;

import android.app.Application;
import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class MyApplication extends Application {

    public static Context AppContenxt;
    private static MyApplication application;
    public static String version;
    //调试模式(打印日志)
    private static boolean DEBUG = true;

    @Override
    public void onCreate() {
        super.onCreate();
        AppContenxt = getApplicationContext();
        application = this;
        version = "v1.0.0";
        RealmConfiguration config = new RealmConfiguration.Builder(this).build();
        Realm.deleteRealm(config);
        Realm.setDefaultConfiguration(config);
//        RxJavaPlugins.getInstance().registerErrorHandler(new RxJavaErrorHandler() {
//            @Override
//            public void handleError(Throwable e) {
//                Log.w("Error", e);
//            }
//        });
    }

    public static Context getAppContenxt() {
        return AppContenxt;
    }

    public static MyApplication getApplication() {
        return application;
    }

}