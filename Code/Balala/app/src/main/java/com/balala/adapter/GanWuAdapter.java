package com.balala.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.balala.R;
import com.balala.func.OnItemClickListener;
import com.balala.network.model.GirlItem;
import com.balala.util.UiHelper;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class GanWuAdapter extends RecyclerView.Adapter<GanWuAdapter.NewsViewHolder> {


    private List<GirlItem> girlsData;
    private OnItemClickListener mOnItemClickListener;
    private Context mContext;
    private boolean animateItems = false;
    private int lastAnimatedPosition = -1;

    public GanWuAdapter(List<GirlItem> mMeizhis, Context context) {
        this.girlsData = mMeizhis;
        this.mContext = context;
    }


    //自定义ViewHolder类
    class NewsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.news_photo)
        ImageView mNewsPhoto;
        @Bind(R.id.news_title)
        TextView mNewsTitle;
        @Bind(R.id.card_view)
        CardView card;
        GirlItem meizhi;

        public NewsViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mNewsPhoto.setOnClickListener(this);
            card.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClickListener.onTouch(v, mNewsPhoto, card, meizhi);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_ganhuo, viewGroup, false);
        NewsViewHolder nvh = new NewsViewHolder(v);
        return nvh;
    }

    @Override
    public void onBindViewHolder(NewsViewHolder viewHolder, int i) {
        runEnterAnimation(viewHolder.itemView, i);
        GirlItem girlItem = girlsData.get(i);
        viewHolder.meizhi = girlItem;
        Glide.with(mContext)
                .load(girlItem.getUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .crossFade() //设置淡入淡出效果，默认300ms，可以传参.crossFade() //设置淡入淡出效果，默认300ms，可以传参
                .into(viewHolder.mNewsPhoto);
        viewHolder.mNewsTitle.setText(girlItem.getDesc());
    }

    @Override
    public int getItemCount() {
        return girlsData.size();
    }

    public void updateItems(List<GirlItem> girlData, boolean animated) {
        animateItems = animated;
        lastAnimatedPosition = -1;
        girlsData.addAll(girlData);
        notifyDataSetChanged();
    }

    private void runEnterAnimation(View view, int position) {
        if (!animateItems || position >= 2) {
            return;
        }
        if (position > lastAnimatedPosition) {
            lastAnimatedPosition = position;
            view.setTranslationY(UiHelper.getScreenHeight(mContext));
            view.animate()
                    .translationY(0)
                    .setStartDelay(100 * position)
                    .setInterpolator(new DecelerateInterpolator(3.f))
                    .setDuration(500)
                    .start();
        }
    }
}