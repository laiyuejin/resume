package com.balala.util;

import com.balala.network.model.GirlItem;
import com.balala.network.model.Image;
import com.balala.network.model.ImageData;
import com.socks.library.KLog;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Func1;

/**
 * 用来统一处理Http的resultCode,并将ImageData的Data部分剥离出来返回给subscriber
 */
public class ImageToMeizhiMapper implements Func1<ImageData,List<GirlItem>> {
    private static ImageToMeizhiMapper INSTANCE = new ImageToMeizhiMapper();

    public ImageToMeizhiMapper() {

    }

    public static ImageToMeizhiMapper getInstance() {
        return INSTANCE;
    }
    @Override
    public List<GirlItem> call(ImageData imageData) {
        KLog.a("inCall");

        List<Image> images = imageData.results;
        List<GirlItem> meizhis = new ArrayList<>(images.size());
        for (Image image : images) {
            GirlItem meizhi = new GirlItem(image.getPublishedAt(),image.getUrl(),image.getDesc());
            meizhis.add(meizhi);
        }
        return meizhis;
    }
}
