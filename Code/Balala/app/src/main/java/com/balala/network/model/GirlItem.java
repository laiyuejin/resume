package com.balala.network.model;

import java.util.Date;


public class GirlItem {
    public  Date mDate;
    public  String url;
    public  String desc;

    public GirlItem(Date date, String url, String desc) {
        mDate = date;
        this.url = url;
        this.desc = desc;
    }

    public Date getDate() {
        return mDate;
    }

    public String getUrl() {
        return url;
    }

    public String getDesc() {
        return desc;
    }
}
