package com.balala.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.balala.R;
import com.balala.network.imp.GetAndroidData;
import com.balala.network.model.GanWuItem;
import com.balala.util.UiHelper;

import java.util.List;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observer;

/**
 * Created by jin on 5/22/2016.
 */
public class AppStart extends Activity {
    private static final String TAG = "Loading Page";
    private static final long LOADING_TIME = 1000;
    private static final int CODE_UPDATE_DIALOG = 0;
    private static final int CODE_URL_ERROR = 1;
    private static final int CODE_NET_ERROR = 2;
    private static final int CODE_ENTER_HOME = 4;
    @Bind(R.id.iv_splash)
    ImageView ivSplash;
    Animation scaleAnimation;
    private static final int ANIMATION_TIME = 2000;

    private static final float SCALE_END = 1.13F;

    private static final int[] IMAGES = {
            R.drawable.splash0,
            R.drawable.splash1,
            R.drawable.splash2,
            R.drawable.splash3,
            R.drawable.splash4,
            R.drawable.splash5,
            R.drawable.splash6,
            R.drawable.splash7,
            R.drawable.splash8,
            R.drawable.splash9,
            R.drawable.splash10,
            R.drawable.splash11,
            R.drawable.splash12,
            R.drawable.splash13,
            R.drawable.splash14,
            R.drawable.splash15,
            R.drawable.splash16,
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appstart);
        ButterKnife.bind(this);
        Random random = new Random(SystemClock.elapsedRealtime());
        ivSplash.setImageResource(IMAGES[random.nextInt(IMAGES.length)]);
        initApp();
        startAnima();

        new Handler().postDelayed(new Runnable() {
            public void run() {
                goHome();
            }
        }, LOADING_TIME);
    }

    /**
     * 初始化工作
     */
    private void initApp() {
        GetAndroidData.getInstance().subscribeData(new Observer<List<GanWuItem>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(List<GanWuItem> items) {
                goHome();
            }
        }, this, 1);

    }

    private void startAnima() {
        ObjectAnimator animatorX = ObjectAnimator.ofFloat(ivSplash, "scaleX", 1f, SCALE_END);
        ObjectAnimator animatorY = ObjectAnimator.ofFloat(ivSplash, "scaleY", 1f, SCALE_END);

        AnimatorSet set = new AnimatorSet();
        set.setDuration(ANIMATION_TIME).play(animatorX).with(animatorY);
        set.start();

        set.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {

                startActivity(new Intent(AppStart.this, MainActivity.class));
                AppStart.this.finish();
            }
        });
    }


    /**
     * 启动
     */
    private void goHome() {
        UiHelper.startToMainActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ivSplash.clearAnimation();
        ivSplash = null;
    }
}