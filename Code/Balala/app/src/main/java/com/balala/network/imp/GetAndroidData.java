package com.balala.network.imp;

import android.content.Context;
import android.support.annotation.NonNull;

import com.balala.globle.Constant;
import com.balala.network.model.GanWuItem;
import com.balala.util.ACache;
import com.balala.util.RandomDatatToItemsMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

/**
 * Created by jin on 5/22/2016.
 */
public class GetAndroidData {
    private static GetAndroidData instance;
    private int dataSource;

    private GetAndroidData() {

    }

    public static GetAndroidData getInstance() {
        if (instance == null) {
            instance = new GetAndroidData();
        }
        return instance;
    }

    public void loadFromNetwork(Context context, int page, BehaviorSubject<List<GanWuItem>> behaviorSubject) {
        Network.getRestAPI(Network.BASE_URL).getRandomData(Constant.ANDROID, page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(RandomDatatToItemsMapper.getInstance()).doOnNext(new Action1<List<GanWuItem>>() {
            @Override
            public void call(List<GanWuItem> datas) { //load on network
                ACache cache = ACache.get(context);
                String json = new Gson().toJson(datas);
                cache.put("android_data" + page, json, 100);   //save to cache
                System.out.println("数据" + datas.size());
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<List<GanWuItem>>() {
            @Override
            public void call(List<GanWuItem> datas) {
                if (behaviorSubject != null)
                    behaviorSubject.onNext(datas);
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    public Subscription subscribeData(@NonNull Observer<List<GanWuItem>> observer, Context context, int page) {
        BehaviorSubject<List<GanWuItem>> behaviorSubject = BehaviorSubject.create();
        behaviorSubject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer);

        Subscription subscription = Observable.create(new Observable.OnSubscribe<List<GanWuItem>>() {
            @Override
            public void call(Subscriber<? super List<GanWuItem>> subscriber) {
                // first load on the android_data
                String json = ACache.get(context).getAsString("android_data" + page);
                if (json == null) { //load one network
                    System.out.println("load on network");
                    loadFromNetwork(context, page, behaviorSubject);
                } else {//load on cache
                    List<GanWuItem> data = new Gson().fromJson(json, new TypeToken<List<GanWuItem>>() {
                    }.getType());
                    System.out.println("load on cache");
                    subscriber.onNext(data);
                    System.out.println("数据" + data.size());
                }
            }
        })
                .subscribeOn(Schedulers.newThread())
                .subscribe(behaviorSubject);
        return subscription;
    }
}
