package com.balala.views;

import android.content.Context;

import com.balala.network.model.GirlItem;

import java.util.List;

/**
 * Created by jin on 9/6/2016.
 */
public interface PrettyView {

    void setRefreshing(boolean b);
    void updateList(List<GirlItem> girlsData, boolean animated);
    Context getContext();
}
