package com.balala.util;

import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

/**
 * Created by jin on 9/10/2016.
 */
public class RxBus {
    private static RxBus mRxBus = null;
    /**
     * PublishSubject只会把在订阅发生的时间点之后来自原始Observable的数据发射给观察者
     */

    private Subject<Object, Object> mRxBusObserverable = new SerializedSubject<>(PublishSubject.create());

    public static synchronized RxBus getInstance() {
        if (mRxBus == null) {
            mRxBus = new RxBus();
        }
        return mRxBus;
    }

    public void post(Object o) {
        mRxBusObserverable.onNext(o);
    }

    public Observable<Object> toObserverable() {
        return mRxBusObserverable;
    }

    /**
     * 判断是否有订阅者
     */
    public boolean hasObservers() {
        return mRxBusObserverable.hasObservers();
    }
}


//example
//public class RxBusActivity extends AppCompatActivity {
//    private CompositeSubscription mCompositeSubscription;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_rx_bus);
//        rxBusObservers();
//        rxBusPost();
//    }
//
//    private void rxBusPost() {
//        Log.d("wxl", "hasObservers=" + RxBus.getInstance().hasObservers());
//        if (RxBus.getInstance().hasObservers()) {
//            RxBus.getInstance().post(new TapEvent());
//        }
//    }
//
//    private void rxBusObservers() {
//        Subscription subscription = RxBus.getInstance()
//                .toObserverable()
//                .subscribe(new Action1<Object>() {
//                    @Override
//                    public void call(Object event) {
//                        if (event instanceof TapEvent) {
//                            //do something
//                            Log.d("wxl", "rxBusHandle");
//                        }
//                    }
//                });
//        addSubscription(subscription);
//    }
//
//
//    public void addSubscription(Subscription subscription) {
//        if (this.mCompositeSubscription == null) {
//            this.mCompositeSubscription = new CompositeSubscription();
//        }
//
//        this.mCompositeSubscription.add(subscription);
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        if (this.mCompositeSubscription != null) {
//            this.mCompositeSubscription.unsubscribe();//取消注册，以避免内存泄露
//        }
//    }
//
//    public class TapEvent {
//    }
//}