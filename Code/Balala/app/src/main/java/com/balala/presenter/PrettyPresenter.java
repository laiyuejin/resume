package com.balala.presenter;

import android.content.Context;

import com.balala.network.imp.GetMeiZhiData;
import com.balala.network.model.GirlItem;
import com.balala.views.PrettyView;

import java.util.List;

import rx.Observer;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by jin on 9/6/2016.
 */
public class PrettyPresenter {
    public CompositeSubscription mCompositeSubscription;
    //    PrettyFragment fragment;
    PrettyView prettyView;
    protected Subscription subscription;
    private int mPage = 1;
    private List<GirlItem> girlsData;
    private Context context;

    public PrettyPresenter(PrettyView view) {
        this.prettyView = view;
        context = view.getContext();
    }

    public void loadData(boolean clean) {
        if(!clean&&mPage!=1)
            mPage++;
        prettyView.setRefreshing(true);
        subscription = GetMeiZhiData.getInstance().subscribeData(new Observer<List<GirlItem>>() {
            @Override
            public void onCompleted() {
                prettyView.setRefreshing(false);
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(List<GirlItem> meizhis) {
                if (clean) girlsData.clear();
                prettyView.updateList(meizhis, true);
                prettyView.setRefreshing(false);
            }
        }, context, mPage);

        this.addSubscription(subscription);
    }

    public void addSubscription(Subscription s) {
        if (this.mCompositeSubscription == null) {
            this.mCompositeSubscription = new CompositeSubscription();
        }
        this.mCompositeSubscription.add(s);
    }

    public void destory(){
        mCompositeSubscription.unsubscribe();
    }
}
