package com.balala.fragment;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.balala.R;
import com.balala.customeview.AbsRecyclerViewAdapter;
import com.balala.network.RetrofitHelper;
import com.balala.network.api.GankApi;
import com.balala.network.model.Gank;
import com.balala.network.model.GankResult;
import com.balala.network.model.Item;
import com.balala.network.model.ZipItem;
import com.balala.activity.GankDetailsActivity;
import com.balala.adapter.GankAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

/**
 * Other part
 * this part show a example of rxjava zip
 */
public class TechFragment extends Fragment {
    @Bind(R.id.recycle)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipe_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;
    private final static String TIME_FORMAT_1 = "yyyy-MM-dd'T'HH:mm:ss.SS'Z'";
    private final static String TIME_FORMAT_2 = "yy/MM/dd HH:mm:ss";
    private final static String EXTRA_TYPE = "type";
    private int pageNum = 30;
    private int page = 1;
    private static final int PRELOAD_SIZE = 6;
    private List<ZipItem> datas = new ArrayList<>();
    private GankAdapter mAdapter;
    private boolean mIsLoadMore = true;
    /**
     * 瀑布流布局
     **/
    private StaggeredGridLayoutManager mLayoutManager;
    private String type;


    public static TechFragment newInstance(String dataType) {
        TechFragment gankFragment = new TechFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_TYPE, dataType);
        gankFragment.setArguments(bundle);
        return gankFragment;
    }

    private View rootView;

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_gank, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initViews();

    }

    public void initViews() {
        type = getArguments().getString(EXTRA_TYPE);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mSwipeRefreshLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onGlobalLayout() {
                mSwipeRefreshLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                mSwipeRefreshLayout.setRefreshing(true);
                startGetBeautysByMap();
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new GankAdapter(mRecyclerView, datas);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(OnLoadMoreListener());
    }

    private void startGetBeautysByMap() {
        GankApi gankApi = RetrofitHelper.getGankApi();
        RetrofitHelper.getGankApi().getBeauties(pageNum, page)
                .doOnSubscribe(new Action0() {

                    @Override
                    public void call() {
                        mSwipeRefreshLayout.setRefreshing(true);
                    }
                })
                .map(new Func1<GankResult, List<Item>>() {

                    @Override
                    public List<Item> call(GankResult gankResult) {
                        List<GankResult.GankBeautyBean> beautys = gankResult.beautys;
                        List<Item> items = new ArrayList<Item>(beautys.size());
                        SimpleDateFormat inputFormat = new SimpleDateFormat(TIME_FORMAT_1);
                        SimpleDateFormat outputFormat = new SimpleDateFormat(TIME_FORMAT_2);
                        int size = beautys.size();
                        for (int i = 0; i < size; i++) {
                            try {
                                Item item = new Item();
                                Date date = inputFormat.parse(beautys.get(i).createdAt);
                                String format = outputFormat.format(date);
                                item.description = format;
                                item.imageUrl = beautys.get(i).url;
                                items.add(item);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        return items;
                    }
                })
                        /** List<Item>,Gank, merge to List<ZipItem>**/
                .zipWith(gankApi.getGankDatas(type, pageNum, page), new Func2<List<Item>, Gank, List<ZipItem>>() {
                    @Override
                    public List<ZipItem> call(List<Item> items, Gank gank) {

                        List<ZipItem> zipItems = new ArrayList<ZipItem>();
                        ZipItem zipItem;
                        List<Gank.AndroidInfo> results = gank.results;

                        for (int i = 0; i < items.size(); i++) {
                            zipItem = new ZipItem();
                            Item item = items.get(i);
                            Gank.AndroidInfo androidInfo = results.get(i);
                            zipItem.imageUrl = item.imageUrl;
                            zipItem.desc = androidInfo.desc;
                            zipItem.description = item.description;
                            zipItem.who = androidInfo.who;
                            zipItem.url = androidInfo.url;
                            zipItems.add(zipItem);
                        }

                        return zipItems;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<ZipItem>>() {

                    @Override
                    public void call(List<ZipItem> zipItems) {

                        datas.addAll(zipItems);
                        finishTask();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
//                        LogUtil.all("数据加载失败");
                        mSwipeRefreshLayout.post(new Runnable() {

                            @Override
                            public void run() {

                                mSwipeRefreshLayout.setRefreshing(false);
                            }
                        });
                    }
                });
    }

    private void finishTask() {
        if (page * pageNum - pageNum - 1 > 0)
            mAdapter.notifyItemRangeChanged(page * pageNum - pageNum - 1, pageNum);
        else
            mAdapter.notifyDataSetChanged();
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
        mAdapter.setOnItemClickListener(new AbsRecyclerViewAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(int position, AbsRecyclerViewAdapter.ClickableViewHolder holder) {
                ZipItem zipItem = datas.get(position);
                if (!type.equals("休息视频")) {
                    //GankDetailsActivity.start(getActivity(), zipItem.url, zipItem.desc,zipItem.imageUrl,zipItem.who);
                    Intent intent = GankDetailsActivity.start(getActivity(), zipItem.url, zipItem.desc, zipItem.imageUrl, zipItem.who);
                    ActivityOptionsCompat mActivityOptionsCompat;
                    if (Build.VERSION.SDK_INT >= 21) {
                        mActivityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                                getActivity(), holder.getParentView().findViewById(R.id.item_img), GankDetailsActivity.TRANSIT_PIC);
                    } else {
                        mActivityOptionsCompat = ActivityOptionsCompat.makeScaleUpAnimation(
                                holder.getParentView().findViewById(R.id.item_img), 0, 0,
                                holder.getParentView().findViewById(R.id.item_img).getWidth(),
                                holder.getParentView().findViewById(R.id.item_img).getHeight());
                    }
                    startActivity(intent, mActivityOptionsCompat.toBundle());
                }
//                else
//                {
//                    VideoWebActivity.launch(getActivity(), zipItem.url);
//                }
            }
        });
    }

    RecyclerView.OnScrollListener OnLoadMoreListener() {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView rv, int dx, int dy) {
                boolean isBottom = mLayoutManager.findLastCompletelyVisibleItemPositions(
                        new int[2])[1] >= mAdapter.getItemCount() - PRELOAD_SIZE;
                if (!mSwipeRefreshLayout.isRefreshing() && isBottom) {
                    if (!mIsLoadMore) {
                        mSwipeRefreshLayout.setRefreshing(true);
                        page++;
                        startGetBeautysByMap();
                    } else {
                        mIsLoadMore = false;
                    }
                }
            }
        };
    }
}
