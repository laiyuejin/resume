package com.balala.network;

import android.content.Context;

import com.balala.network.api.RestAPI;
import com.balala.network.model.GanWuData;
import com.balala.network.model.ImageData;
import com.balala.network.model.RandomData;


import rx.Observable;


public class DataManager {
    private Context mContext;
    private RestAPI mRestAPI;

    public DataManager(Context context, RestAPI restAPI) {
        this.mContext = context;
        this.mRestAPI = restAPI;
    }

    //返回image的信息
    public Observable<ImageData> getImageData(int page) {
        return mRestAPI.getImageData(page);
    }

    //返回每日GanWu的信息
    public Observable<GanWuData> getGanWuData(int year, int month, int day) {
        return mRestAPI.getGanWuData(year, month, day);
    }

    //返回随机的GanWu
    public Observable<RandomData> getRandomData(String type, int page) {
        return mRestAPI.getRandomData(type, page);
    }
}
