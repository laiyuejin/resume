package com.balala.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;

import com.balala.R;
import com.balala.network.imp.Network;
import com.balala.network.model.GanWuData;
import com.balala.network.model.News;
import com.balala.adapter.GanDailyListAdapter;
import com.balala.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 每日的干货信息
 */
public class GanDailyFragment extends BaseFragment {

    private static final String ARG_YEAR = "year";
    private static final String ARG_MONTH = "month";
    private static final String ARG_DAY = "day";
    int mYear, mMonth, mDay;
    List<News> mGanList;
    GanDailyListAdapter mAdapter;
    Subscription mSubscription;
    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @Bind(R.id.stub_empty_view)
    ViewStub mStubEmptyView;

    public static GanDailyFragment newInstance(int year, int month, int day) {
        GanDailyFragment fragment = new GanDailyFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_YEAR, year);
        args.putInt(ARG_MONTH, month);
        args.putInt(ARG_DAY, day);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGanList = new ArrayList<>();
        mAdapter = new GanDailyListAdapter(mGanList);
        parseArguments();
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    private void parseArguments() {
        Bundle bundle = getArguments();
        mYear = bundle.getInt(ARG_YEAR);
        mMonth = bundle.getInt(ARG_MONTH);
        mDay = bundle.getInt(ARG_DAY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recyclerview, container, false);
        ButterKnife.bind(this, rootView);
        initRecyclerView();
        trySetupSwipeRefresh();
        if (mGanList.size() == 0) loadData();
        return rootView;
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    void trySetupSwipeRefresh() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setColorSchemeResources(R.color.refresh_progress_3,
                    R.color.refresh_progress_2, R.color.refresh_progress_1);
            mSwipeRefreshLayout.setOnRefreshListener(
                    new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            loadData();
                        }
                    });
        }
    }

    private void loadData() {
        mSwipeRefreshLayout.setRefreshing(true);
        mSubscription = Network.getRestAPIWithCache(Network.BASE_URL).getGanWuData(mYear, mMonth, mDay)
                .map(data -> data.results)
                .map(this::addAllResults)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> {
                    if (list.isEmpty()) {
                        showEmptyView();
                    } else {
                        mAdapter.notifyDataSetChanged();
                    }
                    mSwipeRefreshLayout.setRefreshing(false);
                }, throwable -> loadError(throwable));
        this.addSubscription(mSubscription);
    }

    private void loadError(Throwable throwable) {
        throwable.printStackTrace();
        Snackbar.make(mRecyclerView, R.string.snap_load_fail,
                Snackbar.LENGTH_LONG).setAction(R.string.retry, v -> {
        }).show();
    }

    private void showEmptyView() {
        mStubEmptyView.inflate();
    }

    private List<News> addAllResults(GanWuData.Result results) {
        if (results.androidList != null) mGanList.addAll(results.androidList);
        if (results.iOSList != null) mGanList.addAll(results.iOSList);
        if (results.appList != null) mGanList.addAll(results.appList);
        if (results.拓展资源List != null) mGanList.addAll(results.拓展资源List);
        if (results.瞎推荐List != null) mGanList.addAll(results.瞎推荐List);
        if (results.休息视频List != null) mGanList.addAll(0, results.休息视频List);
        return mGanList;
    }


    public Context context() {
        return this.getActivity();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
