package com.balala.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.balala.R;
import com.balala.activity.GanDailyActivity;
import com.balala.activity.PictureActivity;
import com.balala.adapter.ScienceNewsAdapter;
import com.balala.base.BaseFragment;
import com.balala.customeview.PullToRefreshView;
import com.balala.network.imp.GetNYTNewsData;
import com.balala.network.model.GirlItem;
import com.balala.network.model.NYTNewsItem;
import com.socks.library.KLog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observer;
import rx.Subscription;

/**
 * Created by jin on 5/23/2016.
 */
public class ScienceNewsFragment extends BaseFragment {
    @Bind(R.id.science_recyclerView)
    RecyclerView mRecyclerView;

    @Bind(R.id.science_refresh_layout)
    PullToRefreshView mPullToRefreshView;

    private boolean mIsFirstTimeTouchBottom = true;
    private static final int PRELOAD_SIZE = 10;

    private int mPage = -1;
    private List<NYTNewsItem> newsData;
    private boolean mMeizhiBeTouched;
    private View view;
    private LinearLayoutManager linearLayoutManager;
    protected Subscription subscription;
    private ScienceNewsAdapter newsAdapter;
    Context context;

    private boolean isLoading = false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this.getActivity();
        newsData = new ArrayList<>();
        newsAdapter = new ScienceNewsAdapter(newsData, context);
        setRetainInstance(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragament_science_list, container, false);
        }
        mPage=-1;

        ButterKnife.bind(this, view);
//        //因为共用一个Fragment视图，所以当前这个视图已被加载到Activity中，必须先清除后再加入Activity
//        ViewGroup parent = (ViewGroup) view.getParent();
//        if (parent != null) {
//            parent.removeView(view);
//        }
        trySetupSwipeRefresh();
        initRecyclerView();
        return view;
    }


    private void initRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(newsAdapter);
        mRecyclerView.addOnScrollListener(getOnBottomListener(linearLayoutManager));
    }

    private void loadData(boolean clean) {
//        mPullToRefreshView.setRefreshing(true);
        KLog.e("开始加载新闻。。。。");
        if(isLoading) {
            KLog.e("已经在加载。。。。");
            setRefreshing(false);
            return;
        }

        isLoading=true;
        mPage += 1;
        subscription = GetNYTNewsData.getInstance().subscribeData(new Observer<List<NYTNewsItem>>() {
            @Override
            public void onCompleted() {
                setRefreshing(false);
                isLoading=false;
            }

            @Override
            public void onError(Throwable e) {
                isLoading=false;
                setRefreshing(false);
                loadError(e);
            }

            @Override
            public void onNext(List<NYTNewsItem> data) {
                if(data==null){
                    Snackbar.make(mRecyclerView,"not more Data",
                            Snackbar.LENGTH_LONG).setAction("Alert!", v -> {

                    }).show();
                    return;
                }
                if (clean) newsData.clear();
                newsAdapter.updateItems(data, true);
                isLoading=false;
            }
        }, getActivity(), mPage);
        addSubscription(subscription);
    }

    void setRefreshing(boolean v){
        if(mPullToRefreshView!=null){
            mPullToRefreshView.setRefreshing(v);
        }
    }

    private void loadError(Throwable throwable) {
        throwable.printStackTrace();
        setRefreshing(false);
        System.out.println(throwable.toString());
        Snackbar.make(mRecyclerView, R.string.snap_load_fail,
                Snackbar.LENGTH_LONG).setAction(R.string.retry, v -> {

        }).show();
    }

    void trySetupSwipeRefresh() {
        if (mPullToRefreshView != null) {
//            mPullToRefreshView.setColorSchemeResources(R.color.refresh_progress_3,
//                    R.color.refresh_progress_2, R.color.refresh_progress_1);
//            mSwipeRefreshLayout.setRe
            mPullToRefreshView.setOnRefreshListener(
                    new PullToRefreshView.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
//                            requestDataRefresh();
                            mPage=-1;
                            loadData(true);
                        }
                    });
        }
    }


    private void startGanDailyActivity(Date publishedAt) {
        Intent intent = new Intent(context, GanDailyActivity.class);
        intent.putExtra(GanDailyActivity.EXTRA_GAN_DATE, publishedAt);
        startActivity(intent);
    }


    private void startPictureActivity(GirlItem meizhi, View transitView) {
        Intent intent = PictureActivity.newIntent(context, meizhi.getUrl(),
                meizhi.getDesc());
        ActivityOptionsCompat optionsCompat
                = ActivityOptionsCompat.makeSceneTransitionAnimation(
                (Activity) context, transitView, PictureActivity.TRANSIT_PIC);
        try {
            ActivityCompat.startActivity((Activity) context, intent,
                    optionsCompat.toBundle());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            startActivity(intent);
        }
    }

    RecyclerView.OnScrollListener getOnBottomListener(LinearLayoutManager layoutManager) {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView rv, int dx, int dy) {
                boolean isBottom =
                        layoutManager.findLastCompletelyVisibleItemPosition() >=
                                newsAdapter.getItemCount() - PRELOAD_SIZE;
                if (!isLoading && isBottom) {
                        loadData(false);
                }
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}
