package com.balala.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.balala.R;
import com.balala.activity.GanDailyActivity;
import com.balala.activity.PictureActivity;
import com.balala.adapter.GanWuAdapter;
import com.balala.base.BaseFragment;
import com.balala.func.OnItemClickListener;
import com.balala.network.model.GirlItem;
import com.balala.presenter.PrettyPresenter;
import com.balala.views.PrettyView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class PrettyFragment extends BaseFragment implements PrettyView {
    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean mIsFirstTimeTouchBottom = true;
    private static final int PRELOAD_SIZE = 10;
    private View view;
    private LinearLayoutManager linearLayoutManager;
    private GanWuAdapter mGanWuAdapter;
    Context context;
    PrettyPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this.getActivity();
        presenter = new PrettyPresenter(this);
        mGanWuAdapter = new GanWuAdapter(new ArrayList<>(), context);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_recyclerview, container, false);
        }
        ButterKnife.bind(this, view);
        setupSwipeRefresh();
        initRecyclerView();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.loadData(false);
    }

    private void initRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mGanWuAdapter);
        mRecyclerView.addOnScrollListener(getOnBottomListener(linearLayoutManager));
        mGanWuAdapter.setOnItemClickListener(getListItemClickListener());
    }

    @Override
    public void setRefreshing(boolean b) {
        if (mSwipeRefreshLayout != null)
            mSwipeRefreshLayout.setRefreshing(b);
    }

    @Override
    public void updateList(List<GirlItem> girlsData, boolean animated) {
        mGanWuAdapter.updateItems(girlsData, true);
    }

    void setupSwipeRefresh() {
        mSwipeRefreshLayout.setColorSchemeResources(R.color.refresh_progress_3,
                R.color.refresh_progress_2, R.color.refresh_progress_1);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        presenter.loadData(true);
                    }
                });
    }

    private OnItemClickListener getListItemClickListener() {
        return (v, meizhiView, card, girlItem) -> {
            if (girlItem == null) return;
            if (v == meizhiView) {
                startPictureActivity(girlItem, meizhiView);
            } else if (v == card) {
                startGanDailyActivity(girlItem.getDate());
            }
        };
    }

    private void startGanDailyActivity(Date publishedAt) {
        Intent intent = new Intent(context, GanDailyActivity.class);
        intent.putExtra(GanDailyActivity.EXTRA_GAN_DATE, publishedAt);
        startActivity(intent);
    }


    private void startPictureActivity(GirlItem meizhi, View transitView) {
        Intent intent = PictureActivity.newIntent(context, meizhi.getUrl(),
                meizhi.getDesc());
        ActivityOptionsCompat optionsCompat
                = ActivityOptionsCompat.makeSceneTransitionAnimation(
                (Activity) context, transitView, PictureActivity.TRANSIT_PIC);
        try {
            ActivityCompat.startActivity((Activity) context, intent,
                    optionsCompat.toBundle());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            startActivity(intent);
        }
    }

    /**
     * scroll bottom event
     *
     * @param layoutManager
     * @return
     */
    RecyclerView.OnScrollListener getOnBottomListener(LinearLayoutManager layoutManager) {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView rv, int dx, int dy) {
                boolean isBottom =
                        layoutManager.findLastCompletelyVisibleItemPosition() >=
                                mGanWuAdapter.getItemCount() - PRELOAD_SIZE;
                if (!mSwipeRefreshLayout.isRefreshing() && isBottom) {
                    if (!mIsFirstTimeTouchBottom) {
                        presenter.loadData(false);
                    } else {
                        mIsFirstTimeTouchBottom = false;
                    }
                }
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        presenter.destory();
    }
    private void loadError(Throwable throwable) {
        throwable.printStackTrace();
        System.out.println(throwable.toString());
        setRefreshing(false);
        Snackbar.make(mRecyclerView, R.string.snap_load_fail,
                Snackbar.LENGTH_LONG).setAction(R.string.retry, v -> {
        }).show();
    }

}
