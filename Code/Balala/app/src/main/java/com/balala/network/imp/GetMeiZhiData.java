package com.balala.network.imp;

import android.content.Context;
import android.support.annotation.NonNull;

import com.balala.network.model.GirlItem;
import com.balala.util.ACache;
import com.balala.util.ImageToMeizhiMapper;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

/**
 * Created by jin on 5/22/2016.
 */
public class GetMeiZhiData {
    private static GetMeiZhiData instance;
    private int dataSource;
    private GetMeiZhiData() {

    }

    public static GetMeiZhiData getInstance() {
        if (instance == null) {
            instance = new GetMeiZhiData();
        }
        return instance;
    }

    public void loadFromNetwork(Context context, int page, BehaviorSubject<List<GirlItem>> behaviorSubject) {
        Network.getRestAPI(Network.BASE_URL).getImageData(page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(ImageToMeizhiMapper.getInstance()).doOnNext(new Action1<List<GirlItem>>() {
            @Override
            public void call(List<GirlItem> meizhis) { //load on network
                ACache cache = ACache.get(context);
                String json = new Gson().toJson(meizhis);
                cache.put("meizhi" + page, json, 100);   //save to cache
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<List<GirlItem>>() {
            @Override
            public void call(List<GirlItem> meizhis) {
                behaviorSubject.onNext(meizhis);
            }
        });
    }

    public Subscription subscribeData(@NonNull Observer<List<GirlItem>> observer, Context context, int page) {
        BehaviorSubject<List<GirlItem>> behaviorSubject = BehaviorSubject.create();
        behaviorSubject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
        Subscription subscription = Observable.create(new Observable.OnSubscribe<List<GirlItem>>() {
            @Override
            public void call(Subscriber<? super List<GirlItem>> subscriber) {
                // first load on the Acache
                String json = ACache.get(context).getAsString("meizhi" + page);
                if (json == null) { //load one network
                    System.out.println("load on network");
                    loadFromNetwork(context, page, behaviorSubject);
                } else {//load on cache
                    List<GirlItem> data = new Gson().fromJson(json, new TypeToken<List<GirlItem>>() {
                    }.getType());
                    System.out.println("load on cache");
                    subscriber.onNext(data);
                }
            }
        })
                .subscribeOn(Schedulers.io())
                .subscribe(behaviorSubject);
        return subscription;
    }
}
