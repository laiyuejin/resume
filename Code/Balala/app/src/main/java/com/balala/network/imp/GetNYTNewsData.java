package com.balala.network.imp;

import android.content.Context;
import android.support.annotation.NonNull;

import com.balala.network.model.NYTImage;
import com.balala.network.model.NYTNewsData;
import com.balala.network.model.NYTNewsItem;
import com.balala.util.ACache;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.socks.library.KLog;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

/**
 * Created by jin on 5/23/2016.
 */
public class GetNYTNewsData {
    private static GetNYTNewsData instance;

    private int dataSource;


    private GetNYTNewsData() {

    }

    public static GetNYTNewsData getInstance() {
        if (instance == null) {
            instance = new GetNYTNewsData();
        }
        return instance;
    }


    public void loadFromNetwork(Context context, int page, BehaviorSubject<List<NYTNewsItem>> behaviorSubject) {
        Network.getRestAPI2(Network.NYT_URL).getNYTSciences("8825d18179f04585bcb083367a01b0ce", 10, page*10)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Func1<NYTNewsData, List<NYTNewsItem>>() {
                         @Override
                         public List<NYTNewsItem> call(NYTNewsData nytNewsData) {
                             if (nytNewsData.status.equals("ERROR")) {
                                 KLog.e("JSON status == error");
                                 return new ArrayList<NYTNewsItem>();
                             }
                             return nytNewsData.results;
                         }
                     }
                ).doOnNext(new Action1<List<NYTNewsItem>>() {
            @Override
            public void call(List<NYTNewsItem> data) { //load on network
                //change the image to bigger one
                if (data == null) {
                    KLog.e("数据为空");
                    return;
                }
                for (int i = 0; i < data.size(); i++) {
                    NYTNewsItem item = data.get(i);
                    boolean haveBig = false;  //filter some news that have not good image
                    for (int j = 0; j < item.images.size(); j++) {
                        NYTImage image = item.images.get(j);
                        if (image != null && image.width == 440) {
                            item.mainImage = image.url;
                            haveBig = true;
                        }
                    }
                    if (item.mainImage == null || item.mainImage.equals("") || haveBig == false) {
                        data.remove(i);
                    }
                }
                ACache cache = ACache.get(context);
                String json = new Gson().toJson(data);
                cache.put("sciences_news" + page, json, 100);   //save to cache
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<List<NYTNewsItem>>() {
            @Override
            public void call(List<NYTNewsItem> data) {
                behaviorSubject.onNext(data);
                behaviorSubject.onCompleted();
            }
        });
    }



    public Subscription subscribeData(@NonNull Observer<List<NYTNewsItem>> observer, Context context, int page) {
        BehaviorSubject<List<NYTNewsItem>> behaviorSubject = BehaviorSubject.create();
        behaviorSubject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
        Subscription subscription = Observable.create(new Observable.OnSubscribe<List<NYTNewsItem>>() {
            @Override
            public void call(Subscriber<? super List<NYTNewsItem>> subscriber) {
                // first load on the Acache
                String json = ACache.get(context).getAsString("sciences_news" + page);
                if (json == null) { //load one network
                    System.out.println("load on network");
                    loadFromNetwork(context, page, behaviorSubject);
                } else {//load on cache
                    List<NYTNewsItem> data = new Gson().fromJson(json, new TypeToken<List<NYTNewsItem>>() {
                    }.getType());
                    System.out.println("load on cache");
                    subscriber.onNext(data);
                    subscriber.onCompleted();
                }
            }
        })
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(behaviorSubject);
        return subscription;
    }
}