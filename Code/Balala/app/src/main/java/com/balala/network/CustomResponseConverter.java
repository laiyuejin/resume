package com.balala.network;

/**
 * Created by jin on 5/28/2016.
 */

import com.balala.network.model.NYTNewsData;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.socks.library.KLog;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Converter;

public class CustomResponseConverter<T> implements Converter<ResponseBody, T> {

    private final Gson gson;
    private final TypeAdapter<T> adapter;

    public CustomResponseConverter(Gson gson, TypeAdapter<T> adapter) {
        this.gson = gson;
        this.adapter = adapter;
    }

    @Override
    public T convert(ResponseBody value) throws IOException {
        try {
            String body = value.string();
            KLog.e(body);
            JSONObject json = new JSONObject(body);
            String   status = json.optString("status");
            if (status.equals("ERROR")) {
                KLog.e("status  errror"+status);
//                String msg = json.optString("errors", "");
                return (T)new NYTNewsData("error",null);
            } else {
                KLog.e("status  good"+status);
                return adapter.fromJson(body);
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        } finally {
            value.close();
        }
    }
}