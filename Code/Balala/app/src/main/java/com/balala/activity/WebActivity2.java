package com.balala.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.balala.R;
import com.balala.customeview.CircleProgressView;
import com.balala.customeview.CommonWebChromeClient;
import com.balala.customeview.CommonWebView;
import com.balala.customeview.CommonWebViewClient;
import com.balala.util.ClipboardUtils;
import com.balala.util.JsHandler;
import com.balala.util.SnackbarUtil;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by jin on 6/4/2016.
 */
public class WebActivity2 extends AppCompatActivity
{
    @Nullable
    @Bind(R.id.circle_progress)
    CircleProgressView mCircleProgressView;
    @Bind(R.id.progress_bar)
    ProgressBar mBar;

    @Bind(R.id.web_view)
    CommonWebView mCommonWebView;

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    private static final String KEY_URL = "key_url";

    private static final String KEY_TITLE = "key_title";

    private String url, title;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);

        //设置布局内容
        setContentView(getLayoutId());
        //初始化黄油刀控件绑定框架
        ButterKnife.bind(this);
//        mCircleProgressView = (CircleProgressView)findViewById(R.id.circle_progress);
        //初始化控件
        initViews(savedInstanceState);
        //初始化ToolBar
        initToolBar();
    }

    public int getLayoutId()
    {

        return R.layout.activity_web2;
    }

    public void initViews(Bundle savedInstanceState)
    {

        Intent intent = getIntent();
        if (intent != null)
            parseIntent(intent);

        initWebSetting();

        hideProgress();
        mCommonWebView.setWebChromeClient(new CommonWebChromeClient(mBar, mCircleProgressView));
        mCommonWebView.setWebViewClient(new CommonWebViewClient(WebActivity2.this));
        mCommonWebView.loadUrl(url);
    }

    public void initToolBar()
    {

        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
        {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(title);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {

        getMenuInflater().inflate(R.menu.menu_web2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        int itemId = item.getItemId();
        switch (itemId)
        {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_share:
                share();
                return true;

            case R.id.action_copy:
                ClipboardUtils.setText(WebActivity2.this, url);
                SnackbarUtil.showMessage(mCommonWebView, "已复制到剪贴板");
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent)
    {

        super.onNewIntent(intent);
        parseIntent(intent);
    }

    private void parseIntent(Intent intent)
    {

        url = intent.getStringExtra(KEY_URL);
        title = intent.getStringExtra(KEY_TITLE);

        if (TextUtils.isEmpty(url))
        {
            finish();
        }
    }

    private void initWebSetting()
    {

        JsHandler jsHandler = new JsHandler(this, mCommonWebView);
        mCommonWebView.addJavascriptInterface(jsHandler, "JsHandler");
    }

    public static boolean start(Context activity, String url, String title)
    {

        Intent intent = new Intent();
        intent.setClass(activity, WebActivity2.class);
        intent.putExtra(KEY_URL, url);
        intent.putExtra(KEY_TITLE, title);
        activity.startActivity(intent);

        return true;
    }

    public static boolean start(Activity activity, String url)
    {

        return start(activity, url, null);
    }


    public void hideProgress()
    {

        mCircleProgressView.setVisibility(View.GONE);
        mCircleProgressView.stopSpinning();
    }


    private void share()
    {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Share");
        intent.putExtra(Intent.EXTRA_TEXT, "from share:" + url);
        startActivity(Intent.createChooser(intent, title));
    }
}

