package com.balala.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.balala.MyApplication;
import com.balala.R;
import com.balala.network.imp.GetAndroidData;
import com.balala.network.model.GanWuItem;
import com.balala.adapter.GanWuListAdapter;
import com.balala.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observer;

/**
 * android article data list fragment  (on main Activity)
 */
public class AndroidFragment extends BaseFragment {

    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    private View view;
    private List<GanWuItem> mNewsList;
    private LinearLayoutManager linearLayoutManager;
    private GanWuListAdapter mGanWuListAdapter;
    private int mPage = 1;
    private boolean mIsFirstTimeTouchBottom = true;
    private static final int PRELOAD_SIZE = 10;



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_recyclerview, container, false);
        }

        ButterKnife.bind(this, view);
        mNewsList = new ArrayList<>();
        mPage=1;
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        trySetupSwipeRefresh();
        linearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mGanWuListAdapter = new GanWuListAdapter(mNewsList, getContext());
        mRecyclerView.setAdapter(mGanWuListAdapter);
        mRecyclerView.addOnScrollListener(getOnBottomListener(linearLayoutManager));
        loadData(false);
    }
    void setRefreshing(boolean v){
        if(mSwipeRefreshLayout!=null){
            mSwipeRefreshLayout.setRefreshing(v);
        }
    }
    private void loadData(boolean clean) {
        setRefreshing(true);
        GetAndroidData.getInstance().subscribeData(new Observer<List<GanWuItem>>() {
            @Override
            public void onCompleted() {
                setRefreshing(false);
            }

            @Override
            public void onError(Throwable e) {
                setRefreshing(false);
                loadError(e);
            }

            @Override
            public void onNext(List<GanWuItem> items) {
                System.out.println(items.size());
                if (clean) mNewsList.clear();
                mGanWuListAdapter.updateItems(items, true);
                setRefreshing(false);
            }
        }, MyApplication.getApplication(), mPage);
    }

    private void loadError(Throwable throwable) {
        throwable.printStackTrace();
        Snackbar.make(mRecyclerView, R.string.snap_load_fail,
                Snackbar.LENGTH_LONG).setAction(R.string.retry, v -> {
            loadData(true);
        }).show();
    }

    void trySetupSwipeRefresh() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setColorSchemeResources(R.color.refresh_progress_3,
                    R.color.refresh_progress_2, R.color.refresh_progress_1);
            mSwipeRefreshLayout.setOnRefreshListener(
                    new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            mPage=1;
                            loadData(false);
                        }
                    });
        }
    }


    public Context getContext() {
        return this.getActivity();
    }



    RecyclerView.OnScrollListener getOnBottomListener(LinearLayoutManager layoutManager) {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView rv, int dx, int dy) {
                boolean isBottom =
                        layoutManager.findLastCompletelyVisibleItemPosition() >=
                                mGanWuListAdapter.getItemCount() - PRELOAD_SIZE;
                if (!mSwipeRefreshLayout.isRefreshing() && isBottom) {
                    if (!mIsFirstTimeTouchBottom) {
                        mSwipeRefreshLayout.setRefreshing(true);
                        mPage += 1;
                        loadData(false);
                    } else {
                        mIsFirstTimeTouchBottom = false;
                    }
                }
            }
        };
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}
