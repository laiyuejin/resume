package com.balala.util;


import com.balala.network.imp.ApiException;
import com.balala.network.model.GanWuItem;
import com.balala.network.model.News;
import com.balala.network.model.RandomData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rx.functions.Func1;


public class RandomDatatToItemsMapper implements Func1<RandomData, List<GanWuItem>> {
    private static RandomDatatToItemsMapper INSTANCE = new RandomDatatToItemsMapper();

    public RandomDatatToItemsMapper() {
    }

    public static RandomDatatToItemsMapper getInstance() {
        return INSTANCE;
    }
    @Override
    public List<GanWuItem> call(RandomData randomData) {
        if (randomData.isError()) {
            throw new ApiException(100);
        }

        List<News> ganwus = randomData.results;
        System.out.println("data"+ganwus.size());
        List<GanWuItem> items = new ArrayList<>(ganwus.size());
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'");
        SimpleDateFormat outputFormat = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
        for (News ganwu : ganwus){
            GanWuItem item = new GanWuItem();
            try {
                Date date = inputFormat.parse(ganwu.getCreatedAt());
                item.date = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
                item.date = "unknown date";
            }
            item.setUrl(ganwu.getUrl());
            item.setWho(ganwu.getWho());
            item.setDescription(ganwu.getDesc());
            items.add(item);
        }
        return items;
    }
}
