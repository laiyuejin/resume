package com.balala.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;

import com.balala.activity.MainActivity;


/**
 * 一个对话框的封装类，会封装更重各样需要的对话框
 * @author 阿进
 *
 */
public class DialogDeal {

	public static void ShowDialog(String msg, Context context) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(msg);
		builder.setCancelable(false);
		builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
	/**
	 * 确定后必须关闭页面的对话框
	 * @param msg
	 * @param context
	 */
	public static void ShowDialogThenFinish(String msg, final Activity context) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(msg);
		builder.setCancelable(false);
		builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				context.finish();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
	public static void ShowDialog(String msg,
			DialogInterface.OnClickListener t, Context context) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(msg);
		builder.setCancelable(false);
		builder.setPositiveButton("register", t);
		builder.setNegativeButton("cancel", null);
		AlertDialog alert = builder.create();
		alert.show();
	}

	public static void RegisterDialog(String msg, final Activity context) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(msg);
		builder.setCancelable(false);
		builder.setPositiveButton("register", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
//				Intent intent = new Intent(context, Register.class);
//				context.startActivity(intent);

			}
		});
		builder.setNegativeButton("cancel", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				Intent intent = new Intent(context, MainActivity.class);
				context.startActivity(intent);
				context.finish();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public static void LoginOutDialog(String msg, final Activity context) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(msg);
		builder.setCancelable(false);
		builder.setPositiveButton("quite", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
//				// 删除账户信息
//				MyApplication.getInstance().getLockPatternUtils()
//						.clearLock();
//				UserDataPreference.deleteUser(context);
//				Intent intent = new Intent(context, Login.class);
//				context.startActivity(intent);

			}
		});
		builder.setNegativeButton("cancel", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
}
