package com.balala.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.balala.R;
import com.balala.network.model.NYTNewsItem;
import com.balala.util.UiHelper;
import com.balala.activity.WebActivity;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by jin on 5/23/2016.
 */
public class ScienceNewsAdapter extends RecyclerView.Adapter<ScienceNewsAdapter.NewsViewHolder>{
    private List<NYTNewsItem> data;
    private Context mContext;
    private boolean animateItems = false;
    private int lastAnimatedPosition = -1;
    public ScienceNewsAdapter(List<NYTNewsItem> Data, Context context) {
        this.data = Data;
        this.mContext = context;
    }

    //自定义ViewHolder类
    class NewsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @Bind(R.id.news_image)
        ImageView mNewsPhoto;
        @Bind(R.id.news_title)
        TextView mNewsTitle;
        @Bind(R.id.news_card_view)
        CardView card;
        @Bind(R.id.news_abstract)
                TextView mNewsAbstract;
        NYTNewsItem itemData;

        public NewsViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mNewsPhoto.setOnClickListener(this);
            card.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mContext.startActivity(WebActivity.newIntent(mContext, itemData.url, itemData.title));
//            WebActivity2.start(mContext, itemData.url, itemData.title);
        }
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.science_news_item, viewGroup, false);
        NewsViewHolder nvh = new NewsViewHolder(v);
        return nvh;
    }

    @Override
    public void onBindViewHolder(NewsViewHolder viewHolder, int i) {
//        runEnterAnimation(viewHolder.itemView, i);
        NYTNewsItem item = data.get(i);
        viewHolder.itemData = item;
        if(item.mainImage!=null||!item.mainImage.equals("")) {
            Glide.with(mContext)
                    .load(item.mainImage)
                    .crossFade() //设置淡入淡出效果，默认300ms，可以传参.crossFade()
                    .into(viewHolder.mNewsPhoto);
        }
        viewHolder.mNewsTitle.setText(item.title);
        viewHolder.mNewsAbstract.setText(item.abstracts);
    }




    @Override
    public int getItemCount() {
        return data.size();
    }

    public void updateItems(List<NYTNewsItem> addData, boolean animated) {
        animateItems = animated;
        lastAnimatedPosition = -1;
        data.addAll(addData);
        notifyDataSetChanged();
    }

    private void runEnterAnimation(View view, int position) {
        if (!animateItems || position >= 2) {
            return;
        }
        if (position > lastAnimatedPosition) {
            lastAnimatedPosition = position;
            view.setTranslationY(UiHelper.getScreenHeight(mContext));
            view.animate()
                    .translationY(0)
                    .setStartDelay(100 * position)
                    .setInterpolator(new DecelerateInterpolator(3.f))
                    .setDuration(700)
                    .start();
        }
    }
}
